\chapter[Introducción]{
  \label{chp:introduccion}
  Introducción
}
\minitoc
\newpage

El proceso de desarrollo del software comienza con un análisis del entorno y
del dominio de la aplicación. En un contexto amplio, se entiende por dominio
como ``una esfera de actividad o interés'', también denominado campo. Es decir,
el área de aplicación donde el software fue creado.

En el presente capítulo se describe el entorno y el dominio de la aplicación, y la
evolución de los servicios de geolocalización en los últimos años. A su vez,
también se exponen los objetivos de este proyecto, dando una visión
de su estructura y las partes que la forman.


\section{Dominio de Aplicación}

La evolución de los \glspl{SIG} ha sido imparable en la ultima década. Motivado 
-sin duda- por el interés de gigantes tecnológicos como Google, Apple y Microsoft, 
ha provocado que se hayan popularizado y extendido tecnologías que  antes sólo 
estaban al alcance de profesionales y científicos. \\

En este sentido, las nuevas arquitecturas de servicios web, diseñadas para
integrar y compartir información multivariada y datos geoespaciales,
propiciando la masificación  social de la georreferenciación, se conocen como
\Gls{geosemantica}. En este ámbito se puede referenciar, el desarrollo de teléfonos
inteligentes o \glspl{smartphone} con funciones de geolocalización por 
satélite que permiten ofrecer a los usuarios información sobre la posición y la 
hora con una gran exactitud, en cualquier parte del mundo, las 24 horas del día. 
El abanico de información disponible comprende distintos servicios tal como
información de tráfico, condiciones climatológicas, eventos o los últimos
juegos de realidad aumentada. \\

Además, el uso de herramientas como Google Earth\textsuperscript{\textregistered}\ ha 
supuesto un salto cualitativo en cuanto a la georreferenciación, con un amplio impacto 
sociológico puesto que se realiza sobre todos los contenidos sociales presentes en el 
mundo. Otro ejemplo, el hecho de poder guardar fotografías de viajes, con pequeñas 
anotaciones adjuntas, permitiendo a los usuarios distribuir y generar presentaciones
fácilmente para ser visualizadas en línea o en un ordenador personal. Este ejemplo encaja
perfectamente en lo que se ha denominado \gls{mashup} o aplicaciones web
híbridas, software que usa y combina datos, presentaciones  y funcionalidad
procedentes de una o más fuentes para crear nuevos servicios.

\begin{figure}[H]
  \centering
  \includegraphics[scale=1.0,keepaspectratio=true]{mashups.jpg}
  \caption[\textit{Mashup} o aplicación web híbrida]{\Gls{mashup} o aplicación web híbrida que combina datos desde fuentes distintas para proveer nuevos servicios}
  \label{fig:mashup}
\end{figure}


\subsection{Objetivos del Proyecto}

El presente proyecto aborda el proceso de creación de una aplicación para generar
presentaciones dinámicas con información geográfica. Mediante la geolocalización
mostrará de una forma visual e interactiva fotografías con datos externos,
pudiendo ser visualizado todo ello en Google Earth/Maps\textsuperscript{\textregistered}.

Para ello se usará una base tecnológica de \gls{codigo abierto} (\textit{open source}) a su 
vez basada en estándares abiertos, donde a su vez el software desarrollado estará siempre 
disponible online para el interés  general a través de una plataforma web.

Además del uso y familiarización con las tecnologías empleadas, otro objetivo
transversal es la introducción en el uso de \glspl{metodologia agil}, basadas
en \glspl{iteracion} cíclicas. Este tipo de metodologías, donde la interacción con los
clientes o usuarios prima por encima de una planificación estática definida al
principio del proyecto, han experimentado un gran auge en los últimos años.



\subsection{Descripción de la Aplicación}

En líneas generales, se desarrollarán los módulos necesarios para proporcionar
las siguientes funcionalidades:

\begin{itemize}
 \item Procesado de fotografías para geoetiquetado de las mismas basándose en sus
 \glspl{metadato} y/o en la información de localización de ficheros \gls{GPX}.
 \item Generación de capas \gls{KML} con las fotografías y datos adicionales, basadas en
 plantillas \gls{XHTML} y editables por el usuario. Las capas KML permitirán realizar
 presentaciones animadas basadas de las fotografías y los datos introducidos.
 \item Arquitectura modular abierta y en capas,  basada en extensiones para permitir a
 usuarios avanzados desarrollar \glspl{plugin} que interactúen con otras aplicaciones.
 \item Facilidad de uso con una interfaz de usuario intuitiva y amigable junto con
 un manual de usuario.
 \item Instalador para el programa o un paquete para su distribución.
\end{itemize}


\section{Organización de la Memoria}

La estructura de esta memoria pretende abarcar todas las fases del proyecto,
teniendo en cuenta la metodología empleada para el  ciclo de vida del software.
Como se describirá en capítulos posteriores, ha seguido un desarrollo basado
en los principios de la \gls{metodologia agil}. \\

Así pues, tras este primer capítulo, en el segundo se desarrolla una \textbf{introducción}
para el lector en el dominio y campo de la aplicación, detallando el auge de los sistemas de
geolocalización y explicando los conceptos necesarios para entender el ámbito y
alcance del software desarrollado. \\

En el tercer capítulo, se procede a realizar un \textbf{plan de proyecto} basado
\textbf{en un análisis de viabilidad} en donde se describe formalmente el
proyecto -objetivos, recursos, metodología, riesgos, etc.- junto con las principales
decisiones preliminares adoptadas. En ese mismo capítulo se analizan los antecedentes,
las características y  diseño de otras tecnologías u otras herramientas similares
que van a rivalizar con este proyecto, es decir, se muestra el estado del arte. \\

En el capítulo \textbf{\ref{chp:tecnologia}} se introduce al lector en la
\textbf{base tecnológica} elegida para el desarrollo del proyecto. Cabe resaltar que,
debido a la multitud de tecnologías tratadas, no se pretende realizar un análisis
profundo de cada una; se tratarán los conceptos básicos que permitan una
comprensión del proyecto. \\

En el siguiente capítulo se muestran las especificaciones y requerimientos
estipulados en el anteproyecto. En base a esas especificaciones se detalla el
\textbf{diseño} alcanzado, donde se explica la metodología del ciclo de vida
elegido, la organización y la estructura del software desarrollado. \\

El capítulo \ref{chp:implementacion}, sobre \textbf{implementación}, 
está dedicado al desarrollo técnico. Se exponen los detalles más importantes de la 
codificación, se muestran las decisiones de implementación tomadas y se explican qué 
algoritmos se aplicaron para resolver ciertos aspectos del diseño. \\

Por último, en el capítulo \textbf{\ref{chp:conclusiones}} se detalla el análisis
económico para estimar los costes de la realización del proyecto. También
se exponen las conclusiones alcanzadas una vez finalizado este trabajo, las
ampliaciones realizadas para nuevas funcionalidades de la propuesta inicial y las
posibles mejoras para versiones futuras. \\

Además, se adjunta un apéndice, que incluye dos casos de uso con el objetivo
de mostrar la apariencia gráfica de los programas desarrollados.

