\chapter[Conceptos Previos]{
  \label{chp:conceptos}
  Conceptos Previos
}
\minitoc
\newpage

En los últimos años, la información geográfica -también llamada información
geoespacial- a cobrado una gran importancia. La facilidad -y necesidad- de
identificar el lugar donde se han obtenido los datos ha provocado la
aparición de nuevos términos. A continuación se presenta una serie de
definiciones que ayudarán a comprender el dominio y alcance de este proyecto.


\section{Coordenadas Geográficas}

Para determinar la posición de un objeto en el globo terrestre se emplean
\gls{coordenadas geograficas}, un sistema de referencia que utiliza  dos
coordenadas angulares: latitud (eje Norte-Sur) y longitud (eje Este-Oeste)
y una tercera coordenada elevación para indicar la altitud.

\begin{description}
 \item[Latitud] es el ángulo que existe entre un punto cualquiera y el
 Ecuador, medida sobre el meridiano que pasa por dicho punto. En cartografía,
 la latitud se suele expresar en grados sexagesimales. Todos los puntos
 ubicados sobre el mismo paralelo tienen la misma latitud. Aquellos que se
 encuentran al norte del Ecuador reciben la denominación Norte (N), los que se
 encuentran al sur del Ecuador reciben la denominación Sur (S).
 El rango de valores es de 0º para el Ecuador a 90º para los polos Norte y Sur
 con latitud 90º N y 90º S respectivamente.
 \item[Longitud] es el ángulo a lo largo del ecuador desde cualquier punto de
 la Tierra. Por convenio, está aceptado internacionalmente que Greenwich en Londres
 es la longitud 0. Las líneas de longitud son círculos máximos que pasan por los
 polos y se llaman meridianos. La longitud también se expresa en grados
 sexagesimales entre 0º y 180º indicando a qué hemisferio (occidental W —del
 inglés \textit{West}— y oriental E —\textit{East}—) o también entre 0º y 180º positivos
 indicando Este, o negativos indicando hacia el Oeste.
 \item[Elevación] o \textbf{altitud} es la distancia vertical de un punto de la Tierra
 respecto al nivel del mar, llamada elevación sobre el nivel medio del mar, en contraste
 con la altura, que indica la distancia vertical existente entre dos puntos de la
 superficie terrestre.
\end{description}

Combinando estos dos ángulos (latitud y longitud) junto con la elevación, es posible
expresar la posición de cualquier punto de la superficie de la Tierra.


\subsection{Sistema Global de Navegación por Satélite}

Para la adquisición de las coordenadas geográficas se usan los Sistemas Globales
de Navegación por Satélite más conocidos por su acrónimo en inglés \gls{GNSS}.

Un GNSS es una constelación de satélites que transmite rangos de señales
utilizados para el posicionamiento y localización en cualquier parte del globo
terrestre. El cálculo de una posición sobre la superficie  terrestre se realiza
midiendo las distancias de un mínimo de tres satélites de posición conocida, un
cuarto satélite aportará, además, la altitud. En la práctica, un receptor
capta las señales de sincronización emitidas por los satélites que contienen
la posición del satélite y el tiempo exacto en que ésta fue transmitida.
La precisión de las mediciones de distancia determina la exactitud de la
ubicación final y pueden ser mejoradas con sistemas de apoyo de radionavegación
en tierra.

Actualmente, el Sistema de Posicionamiento Global (\gls{GPS}) de los Estados
Unidos de América y el Sistema Orbital Mundial de Navegación por Satélite (\gls{GLONASS})
de la Federación Rusa son los únicos sistemas completos que forman parte del concepto
GNSS. Con muchos años de retraso, la Unión Europea está desarrollando su
propio sistema de navegación por satélite conocido como Galileo. También la
República Popular China está construyendo el sistema Beidou.

Aunque el GPS de EEUU es el sistema más utilizado, este proyecto es independiente
del hardware de navegación, puesto que solamente se limitará a procesar los datos
de salida de los dispositivos de recepción. Como se verá en la siguiente sección,
dichos datos de salida están estandarizados.


\section{Geomática: Geoetiquetado}

\Gls{geomatica} es el término que hace referencia a un conjunto de ciencias en las
cuales se integran los medios para la captura, tratamiento, análisis,
interpretación, difusión y almacenamiento de información geográfica. Este
término ya es parte de las normas de estandarización \acrshort{ISO}.

La parte de la Geomática definida como el proceso de agregar información
geográfica en los \glspl{metadato} de  ficheros de imágenes, vídeos, audio, sitios
web, etc. se conoce como \textbf{\gls{geoetiquetado}}. Por lo general estos datos 
son coordenadas geográficas que definen el lugar en el que el fichero ha sido 
generado, aunque también puede incluir nombre del lugar, calle, código postal, etc. 
para posteriormente realizar la geocodificación.

Se entiende por \textbf{\gls{geocodificacion}} al proceso de asignar coordenadas 
geográficas (latitud, longitud, altitud) a puntos del mapa (direcciones, puntos de 
interés, etc.). Las coordenadas geográficas producidas pueden ser usadas para localizar 
la posición en un \gls{SIG}.

Las geoetiquetas permiten a los usuarios encontrar información sobre un lugar
específico. Así, por ejemplo, es posible hallar imágenes tomadas próximas a un
sitio determinado mediante la introducción en un buscador de sus coordenadas
geográficas.

\textbf{\Gls{georreferenciacion}} es un neologismo que hace referencia al posicionamiento con el
que se define la localización de un objeto espacial en un sistema de coordenadas y
\emph{\gls{datum}}\footnote{Un sistema de referencia geodésico es un modelo matemático
que permite asignar coordenadas a puntos sobre la superficie terrestre sin usar otro
sistema de referencia. El \textit{datum} mas común es WGS84 de inglés de World Geodetic System de 1984.}
determinado. Este proceso es utilizado frecuentemente en los Sistemas de
Información Geográfica para establecer relaciones entre objetos y su posición.


\section{Formatos de Datos}

A continuación se describen brevemente los formatos de los datos que se emplearán en
el desarrollo del proyecto.

\subsection{Exif: \textit{Exchangeable Image File Format}}

\acrlong{Exif} (abreviado \textit{\acrshort{Exif}}) es una especificación de \glspl{metadato}
estándar para formatos de archivos multimedia. Los formatos de imagen tales como
\gls{JPEG}, \gls{TIFF}, etc e incluso otros formatos de audio como \gls{WAVE}, 
disponen de una definición para permitir guardar metadatos tales como información de fecha 
y hora, modelo y fabricante del dispositivo, configuración de la cámara (por ejemplo
apertura, velocidad del obturador, distancia focal, etc.) y lo más interesante para
este proyecto, información de las coordenadas geográficas (latitud, longitud y altitud). \\

El programa usará las etiquetas Exif en el proceso de geoetiquetado para agregar
información geográfica en las fotografías de forma que queden georreferenciadas.


\subsection{Formato de Intercambio GPS: GPX}

La mayoría de los sistemas receptores de los \gls{GNSS} de posicionamiento global,
utilizan un formato estándar para guardar todos los datos de posicionamiento
y localización. \\

\Gls{GPX} (\textit{\acrlong{GPX}}) es la definición de un esquema \acrshort{XML} 
pensado para transferir datos \acrshort{GPS} entre aplicaciones. Se puede usar para 
referenciar puntos (\glspl{waypoint}), recorridos (\glspl{track}), y rutas (\glspl{route}). 
A pesar de su nombre, no es dependiente del sistema de posicionamiento GPS, es un formato 
estándar y abierto. Puede ser usado por receptores de otros sistemas de posicionamiento 
como \gls{Galileo}, \acrshort{GLONASS}, etc. \\

\begin{lstlisting}[language=xml,numbers=none,caption={Ejemplo de documento GPX},label={lst:gpx}]
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>

<gpx xmlns="http://www.topografix.com/GPX/1/1" xmlns:gpxx="http://www.garmin.com/xmlschemas/GpxExtensions/v3" xmlns:gpxtpx="http://www.garmin.com/xmlschemas/TrackPointExtension/v1" creator="Oregon 400t" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd http://www.garmin.com/xmlschemas/GpxExtensions/v3 http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd http://www.garmin.com/xmlschemas/TrackPointExtension/v1 http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd">
  <metadata>
    <link href="http://www.garmin.com">
      <text>Garmin International</text>
    </link>
    <time>2009-10-17T22:58:43Z</time>
  </metadata>
  <trk>
    <name>Example GPX Document</name>
    <trkseg>
      <trkpt lat="47.644548" lon="-122.326897">
        <ele>4.46</ele>
        <time>2009-10-17T18:37:26Z</time>
      </trkpt>
      <trkpt lat="47.644548" lon="-122.326897">
        <ele>4.94</ele>
        <time>2009-10-17T18:37:31Z</time>
      </trkpt>
      <trkpt lat="47.644548" lon="-122.326897">
        <ele>6.87</ele>
        <time>2009-10-17T18:37:34Z</time>
      </trkpt>
    </trkseg>
  </trk>
</gpx>
\end{lstlisting}

En un documento GPX, un \gls{waypoint} es una etiqueta georeferenciada sin
ninguna otra relación con otros objetos. Una colección ordenada en el tiempo de
\textit{waypoints} es un \gls{track}, describiendo lo que se conoce como 
camino o recorrido. Si los puntos que conforman un recorrido no tienen tiempos asociados, 
es decir, sólo están ordenados por localización, entonces es una ruta, \gls{route}.

Las propiedades mínimas para cada punto son la longitud y la latitud expresadas
en grados decimales usando el \gls{datum} WGS84. Los tiempos están definidos
en Tiempo Universal Coordinado \gls{UTC} usando el formato ISO8601.


\subsection{Lenguaje de Marcado Geográfico KML}

\emph{\gls{KML}} (del acrónimo en inglés \textit{\acrlong{KML}}) es un lenguaje de 
marcado basado en \acrshort{XML} para representar datos geográficos en tres dimensiones. Se usa
para modelos, transporte y almacenamiento de información geográfica entre
los diferentes software que hacen uso de este tipo de datos, como los \glspl{SIG}.

Fue creado para ser manejado con Keyhole LT, un software \acrshort{SIG} desarrollado
por la compañía con mismo nombre, y tras la adquisición por parte de Google
de la compañía en 2004, pasó a denominarse Google Earth, siendo este programa
el software más popular para el manejo KML. Es un formato estándar a partir de
las especificaciones KML 2.2 aprobado por el \acrlong{OGC} el
14 de abril de 2008.

Un fichero KML especifica un conjunto de localizaciones y características
(lugares, imagenes, polígonos, etc.) para el software visualizador.
Cada localización tiene un identificador y sus coordenadas (latitud y longitud),
pero también puede contener información adicional que permite especificar
descripciones, puntos de vista, movimientos de cámara, creación de estructuras 3D,
etc. \\

\begin{lstlisting}[language=xml,numbers=none,caption={Ejemplo de documento KML sencillo},label={lst:kml}]
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Document>
    <Placemark>
      <name>Google Earth - New Placemark</name>
      <description>Some Descriptive text.</description>
      <LookAt>
        <longitude>-90.86879847669974</longitude>
        <latitude>48.25330383601299</latitude>
        <range>440.8</range>
        <tilt>8.3</tilt>
        <heading>2.7</heading>
      </LookAt>
      <Point>
        <coordinates>-90.86948943473118,48.25450093195546,0</coordinates>
      </Point>
    </Placemark>
  </Document>
</kml>
\end{lstlisting}

Además de las características mencionadas, existen otras extensiones al estándar
-como la que se va a explotar en este proyecto- que permiten añadir nuevas funcionalidades. \\

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.7,keepaspectratio=true]{kml.png}
  \caption[Esquema de Objetos de KML]{KML con la extensión de nombres {\color{red}\textbf{gx}}}
  \label{fig:kml}
\end{figure}

En particular, la extensión \textit{Touring} especifica un nuevo espacio de nombres
definido por el prefijo \textit{gx:*} que permite:

\begin{itemize}
 \item Definir vuelos y rutas aéreas entre localizaciones.
 \item Programar transiciones suaves a vista de pájaro entre puntos.
 \item Reproducir audio en puntos predefinidos.
 \item Actualizaciones dinámicas del contenido.
\end{itemize}

Además, los documentos \gls{KML} se pueden comprimir en formato \gls{zip} 2.0 generando
un fichero \gls{KMZ} donde todos los recursos (imágenes, modelos 3D, audio, etc)
están contenidos y enlazados internamente. Está característica es la que explotará
el programa a desarrollar para generar las presentaciones. \\

Por supuesto, el visualizador de referencia para este proyecto, Google Earth 5.X\textsuperscript{\textregistered}\  
soporta todas estas características.






