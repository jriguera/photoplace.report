\chapter[Tecnología]{
  \label{chp:tecnologia}
  Tecnología
}
\minitoc
\newpage

La base tecnológica es el grupo de tecnologías sobre las que se apoya el
software y su desarrollo. Constituyen una parte importante en el análisis
del proyecto, puesto que, de su elección dependen otros factores tales como:
curva de aprendizaje y facilidad de uso, soporte y evolución futura,
documentación y comunidad, etc.

Este capítulo se centra en la última versión, a fecha de entrega de esta memoria,
del programa. Es probable que algunas módulos aquí expuestos sean sustituidos
por otros en futuras \glspl{iteracion} siguiendo el ciclo de vida del software.


\section{Base Tecnológica}

A continuación se describen las tecnologías más importantes sobre las que
se ha desarrollado el programa \textbf{\software}. El nexo común entre todas las 
tecnologías expuestas es su capacidad multiplataforma y formar parte del ecosistema del
software libre.


\subsection{Python}

Python es un lenguaje de alto nivel, multiplataforma y multipropósito. Su
filosofía de diseño enfatiza en la claridad y legibilidad del código. Es un
lenguaje muy extendido que dispone de muchas librerías y frameworks de
desarrollo. \\

Aunque Python es un lenguaje interpretado, usa un proceso en dos pasos para
compilar y generar código intermedio que posteriormente ejecutará una máquina
virtual. Es un lenguaje con tipos de datos y resolución de nombres dinámica
con contadores de referencias a objetos para administrar dinámicamente la
memoria. Otra característica importante es que soporta múltiples paradigmas de
programación: orientación a objetos, programación imperativa y, en menor medida,
programación funcional. \\

Además de la \textit{Python Standard Library}, la librería estándar de Python, que
provee todos los métodos básicos del lenguaje, este proyecto emplea:

\begin{description}
 \item[pyExiv2]\footnote{Disponible en \url{http://tilloy.net/dev/pyexiv2}.} 
 es un \textit{binding}, un módulo para Python para enlazar la librería
 \textit{libexiv2} implementada en C++. Es una interfaz de alto nivel para todas las
 funcionalidades ofrecidas por la librería raíz, lo cual permite manipular fácilmente
 desde Python los \glspl{metadato} -en particular las etiquetas \gls{Exif}- de los ficheros \gls{jpeg}.
 \item[Pillow]\footnote{Web oficial: \url{http://pillow.readthedocs.org}.} 
 es una librería para la manipulación de imágenes. Surgió como a raíz de
 la antigua \gls{PIL} y provee funciones para la manipulación de imágenes: escalado, 
 efectos especiales, etc.
 \item[py2exe]\footnote{Más información en \url{http://sourceforge.net/projects/py2exe}.}
 es una extensión que convierte un programa Python en un ejecutable para
 plataformas MicroSoft Windows. Estos ejecutables puede ejecutarse sin necesidad de
 tener Python instalado en el sistema.
 \item[Gettext] es una herramienta para proveer el soporte de internacionalización y
 localización (\gls{I18N}\footnote{Abreviación del inglés, \textit{internationalization},
 entre la primera letra \textit{i} y la última \textit{n} hay 18 letras.}).
 Se generan ficheros con las cadenas de texto del programa, se traducen a los
 distintos idiomas y se compilan. Gracias a esta extensión, el programa cargará
 la versión compilada de acuerdo al idioma del usuario. Permite desacoplar las
 tareas de traducción de las de codificación.
\end{description}

Sin embargo existen otros dos módulos con un peso mayor en el proyecto que se describen
en las siguientes secciones.

\begin{description}
 \item[PyGTK], una adaptación de la biblioteca gráfica \gls{GTK} para Python.
 \item[Django] es un \textit{framework} en Python para desarrollo web.
\end{description}

Este proyecto emplea la rama 2.x de Python, en la última versión estable 2.7,
pero desde el principio se ha procurado un desarrollo teniendo en cuenta una
futura migración a la rama 3.x. Las razones por la que se ha usado esta rama son:
\begin{itemize}
 \item El soporte de Python 3.x todavía es secundario en muchos programas, de forma
 que la rama 2.x es la rama preferida.
 \item Las distribuciones de Linux incluyen Python 2.x por defecto.
 \item Está soportada oficialmente por la fundación Python.
 \item Python 3.x no ofrece mayor rendimiento que la rama 2.x.
\end{itemize}


\subsection{PyGTK}

PyGTK es un \textit{binding}, una adaptación de la biblioteca gráfica \gls{GTK} para
Python. GTK+ es una potente librería usada para desarrollar el entorno gráfico GNOME, 
así como otras muchas aplicaciones. Esta biblioteca permite el desarrollo sencillo de 
la \gls{GUI} respetando a su vez el patrón \gls{MVC}.

Para diseñar las interfaces gráficas en GTK+, la herramienta Glade\footnote{Disponible en \url{https://glade.gnome.org}}
genera un fichero \gls{xml} (formato conocido como GtkBuilder) que permite definir 
todos los elementos de la interfaz de usuario.De esta forma, la posición, apariencia y 
formato de los \glspl{widget} se puede modificar sin necesidad de reescribir el código.

Todas las interfaces GTK+ de este proyecto están definidas mediante ficheros
XML que han sido generado usando la herramienta Glade.


\subsection{Paquetes de Distribución e Instalador}

El software dispone de un instalador para sistemas Windows y paquetes para Ubuntu.
La base de este instalador son las herramientas Python \textit{setuptools}, una
extensión que permite definir todas las acciones para: generar paquetes comprimidos
(\gls{zip}, \gls{tgz}),
paquetes \gls{PIP}\footnote{Sistema gestor de paquetes de Python. \url{https://pypi.python.org/pypi}},
gestionar la instalación y desinstalación directamente en el sistema,
etc. Es un sistema extensible, es decir, aunque no existan paquetes para un
sistema (en este caso para Ubuntu), se pueden generar fácilmente o gestionar
directamente la instalación con \textit{setuptools} (siempre que sea un sistema
soportado). De echo, \textit{py2exe} es una extensión de \textit{setuptools} que
permite realizar esas acciones en sistemas Windows y generar ejecutables. \\

Sin embargo, aunque para la plataforma Windows se genere un ejecutable, se
necesita un paso adicional: generar un instalador. El instalador es el encargado
de copiar todos los recursos (iconos, plantillas, ficheros \gls{I18N}, etc.)
a los directorios predefinidos, generar entradas en el registro y accesos directos.
\gls{nsis}\footnote{\textit{Nullsoft Scriptable Install System} \url{http://nsis.sourceforge.net}}
es un programa abierto y libre para generar dichos instaladores, con las siguientes
características:

\begin{itemize}
 \item \gls{GUI}, Interfaz gráfica de usuario moderna, especificada en 
 distintos pasos, basada en \textit{wizard}\footnote{Asistente  en una interfaz gráfica 
 que presenta al usuario una secuencia de ventanas que conducen al usuario por los 
 pasos necesarios para configurar fácilmente un proceso.},
 flexible y con muchos componentes.
 \item Soporte para instalación multilenguaje y en todas las plataformas Windows.
 \item Múltiples extensiones: escribir en el registro, descargar componentes de
 Internet, comunicarse con servicios de Windows, etc.
 \item Compresión del paquete de software.
 \item Basado en un lenguaje de script sencillo, con amplia documentación.
\end{itemize}


\subsection{Django}

Para la versión web del proyecto se ha usado Django, un framework de
desarrollo para aplicaciones web que sigue el patrón de diseño \gls{MVC}
que consiste básicamente, en separar y aislar los componentes de la aplicación. 
Sin embargo, Django usa una variante de este patrón conocido como \gls{MTV} 
(Modelo-Plantilla-Vista), donde el enlace con el clásico patrón sería 
Modelo-Modelo, Vista-Plantilla, Controlador-Vista.

Los diferentes componentes son:
\begin{itemize}
 \item \textbf{Modelo}. Los datos persistentes de la aplicación, todo lo relacionado
 con la base de datos y la descripción de las tablas se realiza usando objetos.
 \item \textbf{Plantilla}. La interfaz de usuario que define cómo se van a mostrar los datos
 al usuario y los casos de uso, cuál va a ser su disposición en la pantalla y
 qué componentes gráficos se van a usar.
 \item \textbf{Vista} es la parte encargada de procesar y ordenar los datos obtenidos
 del modelo o bien ingresados por el usuario.
\end{itemize}


\subsubsection{JavaScript: jQuery y AJAX}

jQuery es la biblioteca de JavaScript más usada en entorno web. Permite
simplificar la manera de interactuar con los documentos \gls{XHTML}, manipular el
árbol \gls{dom}\footnote{\textit{\acrlong{DOM}} (Modelo de Objetos del Documento), 
una API que proporciona un conjunto estándar de métodos para representar 
documentos XHTML}, manejar eventos, desarrollar animaciones y agregar interacción
usando peticiones \gls{AJAX}. 

Características principales:

\begin{itemize}
 \item Selección y modificaciones del árbol \gls{DOM} incluyendo soporte para \gls{CSS}3.
 \item Soporte para eventos en el navegador web y \gls{ajax} para mejorando la 
 interactividad, velocidad y usabilidad en la aplicación.
 \item Multiplataforma: es compatible con la gran mayoría de navegadores web,
 incluso algunos dispositivos móviles proveen funciones para asistir sus
 funciones.
 \item Ampliable mediante extensiones y \glspl{plugin}, como se mostrará a continuación.
\end{itemize}

En cuanto a las extensiones y \glspl{plugin} usados en el proyecto:
\begin{itemize}
 \item \textbf{JQuery-UI} para manejar los \glspl{widget}, animaciones y efectos visuales.
 \item \textbf{GMaps3} para crear y manejar objetos en Google Maps.
 \item \textbf{Fileupload} para gestionar la subida de ficheros automáticamente.
\end{itemize}

\paragraph{AJAX,} acrónimo de \textit{\acrfull{AJAX}}
(JavaScript asíncrono y XML), es una técnica de desarrollo web para crear
aplicaciones interactivas que se ejecutan en el navegador del usuario
mientras se mantiene la comunicación asíncrona con el servidor en segundo
plano. De esta forma es posible realizar actualizaciones del contenido de las
páginas \gls{xhtml} sin necesidad de recargarlas, mejorando la interactividad, velocidad
y usabilidad en las aplicaciones. Para ello, usando las funciones de jQuery, se
intercambian los objetos valor entre la \gls{GUI} en el navegador con el
servidor web en formato \gls{JSON} usando el protocolo \gls{HTTP}.


\subsubsection{Bootstrap}

Bootstrap\footnote{Más información en el sitio web del proyecto: \url{http://getbootstrap.com}} 
es un framework que simplifica el proceso de creación de diseños
web combinando \gls{css} y JavaScript desarrollado por Twitter. Es uno de las
herramientas más utilizadas en el diseño de sitios y aplicaciones web, por
su riqueza en plantillas para formularios, botones, menús, tipografía, etc.
\newpage

Bootstrap destaca por:

\begin{itemize}
 \item Permite crear interfaces adaptables a diferentes navegadores,
 tanto de escritorio como a dispositivos móviles a distintas escalas y resoluciones.
 Esta característica se denomina diseño adaptativo o \textit{Responsive Design}.
 \item Como contrapartida al punto anterior, tiene un soporte relativamente incompleto
 para HTML5 y CSS3, pero compatible con la mayoría de los navegadores web.
 \item Integración con JavaScript, en particular con jQuery.
 \item Framework ligero que se integra de forma fácil, con muchos componentes
 y con amplia documentación, lo que permite ahorrar tiempo y esfuerzo a la hora
 de diseñar una interfaz web de usuario.
\end{itemize}


\section{Tecnologías de desarrollo}

Además de las librerías sobre las que se apoya el software, existen otras herramientas
que forman parte del entorno de desarrollo. La mayoría de las veces configurar y mantener
el entorno de desarrollo no es tarea sencilla, sobre todo si se trata de aplicaciones
multiplataforma con un equipo de desarrolladores distribuido.


\subsection{Glade}

\textit{Glade Interface Designer}, en castellano "Diseñador de interfaces Glade" es una herramienta 
de desarrollo visual de interfaces gráficas mediante \gls{GTK}/GNOME. Es independiente del 
lenguaje de programación, puesto que no genera código fuente sino un archivo \gls{xml} que 
define la posición y eventos de todos los \glspl{widget}. Esto permite modificar la \gls{GUI}
sin cambiar el código fuente, contribuyendo al desarrollo de la \gls{arquitectura en capas}. 
GtkBuilder es un formato XML que Glade usa para almacenar los elementos de las interfaces 
diseñadas. Estos archivos pueden emplearse para construirla en tiempo de ejecución mediante 
el objeto GtkBuilder de GTK+. 


\subsection{Vagrant}

Vagrant\footnote{En la web oficial \url{http://www.vagrantup.com} se puede encontrar más información.} 
es una aplicación que facilita considerablemente la creación de entornos virtuales de desarrollo. 
Para ello Vagrant abstrae el sistema de virtualización subyacente (normalmente VirtualBox) y permite 
instalar y configurar el software de la máquina virtual con herramientas de automatización como 
Chef, Puppet o incluso los clásicos \textit{scripts shell}.

Gracias a Vagrant es posible generar imágenes para distintas distribuciones, con todas
las librerías y dependencias instaladas, de forma que todos los miembros del equipo
de desarrollo disponen de una copia exacta del entorno.


\subsection{Git-Flow}

Git\footnote{Web oficial disponible en \url{http://git-scm.com}} es un sistema 
distribuido de control de código fuente o \gls{scm}\footnote{Source Code Management}. 
Permite a los desarrolladores trabajar con distintas versiones, aplicar parches, auditar el 
código fuente, recuperar versiones anteriores, definir etiquetas, seguridad ante 
modificaciones no deseadas y, la más importante:  gestión avanzada (y eficiente) de ramas 
(\textit{branchs}) y uniones (\textit{merging}). \\

Git-Flow\footnote{Más información en \url{http://nvie.com/posts/a-successful-git-branching-model}} 
es una colección de extensiones para Git que proveen una serie de operaciones de alto nivel 
para utilizar un repositorio Git de manera más fácil. Mediante el uso de de \textit{branchs} 
y \textit{merging}, define un \textbf{flujo de trabajo} basado en las siguientes operaciones:

\begin{itemize}
 \item \textbf{\textit{Feature}} gestiona el desarrollo día a día del código fuente. Cada funcionalidad
 acordada en la iteración debe corresponder con al menos una \textit{feature}. 
 \item \textbf{\textit{Release}} para gestionar las versiones estables del software (iteraciones).
 \item \textbf{\textit{Hotfix}} gestiona los parches, para aplicar \textit{bugs} tanto en las ramas de 
 desarrollo como en las versiones estables.
\end{itemize}

Usando este flujo de trabajo y herramientas como Vagrant, es posible gestionar equipos
grandes de desarrolladores trabajando en paralelo. Además, aunque en este proyecto no
se han usado, es posible conectar las ramas con sistemas de Integración Continua como
Jenkins, permitiendo automatizar pruebas, generar documentación, etc.


