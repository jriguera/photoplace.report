\chapter[Diseño]{
  \label{chp:disenho}
  Diseño
}
\minitoc
\newpage

Como se ha visto, la realización del proyecto ha seguido directrices marcadas
por la \gls{metodologia agil} \gls{Scrum-Ban} y por el \gls{ddd}. Además,
la arquitectura de la aplicación se ha dividido en distintas capas apoyadas por
patrones de diseño e integración.

En este capítulo se muestran, en primer lugar, los conceptos básicos necesarios
para comprender el proceso de diseño del software y, posteriormente, los modelos
del software logrados tras la última iteración de la aplicación.


\section{Ciclo de vida}

El ciclo de vida está basado en los principios de las \glspl{metodologia agil} y
se organiza en \glspl{iteracion} que incluyen: análisis de requisitos,
diseño, codificación, revisión y documentación. \\

En cada iteración, un tiempo establecido en dos semanas, se desarrollan las
historias de usuario elegidas previamente. Las historias de usuario son la
técnica utilizada en \Gls{scrum} para especificar los requisitos del software,
lo que equivaldría a los casos de uso en el proceso unificado. Se describen
brevemente las características que el sistema debe tener desde la perspectiva del
cliente, se definen las tareas, las pruebas, la documentación y por último
unos criterios de aceptación que permiten determinar si la historia se ha
completado.

La planificación de las iteraciones se puede realizar basándose en el tiempo o 
el alcance. La velocidad del proyecto es utilizada para establecer cuántas historias 
se pueden implementar antes de una fecha determinada o cuánto tiempo tomará
implementar un conjunto de historias. Al planificar por tiempo, se multiplica
el número de iteraciones por la velocidad del proyecto, determinándose cuántos
puntos se pueden completar. Al planificar según el alcance del sistema, se divide la
suma de puntos de las historias de usuario seleccionadas entre la velocidad del
proyecto, obteniendo el número de iteraciones necesarias para su implementación.


\subsection{Bases Conceptuales del Diseño}

Cumplir los requisitos mínimos definidos en el anteproyecto exige desarrollar
un sistema con un diseño basado en:

\begin{description}
 \item[Sistema modular.] Se puede definir sistema software modular como aquel
que ayuda a los diseñadores a construir sistemas formados por elementos
autónomos y organizados en arquitecturas sencillas.
 \item[Principio de ocultación de información] Los módulos de un sistema deben
diseñarse de modo que la información contenida en ellos sea inaccesible a
todos aquellos módulos que no necesiten tal información.
 \item[Abstracción de datos.] Una abstracción de datos está formada por un
conjunto de objetos y un conjunto de operaciones (abstracciones funcionales
que manipulan estos objetos). Son modelos del dominio.
 \item[\gls{api} explícita.] Una API es explícita si el nombre identificador de las
funciones que forman parte de la API da al desarrollador una idea intuitiva de
la funciones que desempeña, es decir definir una convección de nombres para
los objetos.
 \item[\gls{GUI} intuitiva.] Una interfaz gráfica de usuario intuitiva es aquella que
logra máxima funcionalidad con mínimas funciones de manipulación e interacción
por parte del usuario.
 \item[Sistema escalable.] Es aquel que su arquitectura permite de forma
sencilla ampliar prestaciones evitando modificaciones de la base estructural.
\end{description}


\subsection{Planificación de las Iteraciones}

Tomando los requerimientos del software definidos en el~\nameref{chp:plan},
se crean las historias de usuario y se realiza una planificación en cinco
iteraciones basándose en el tiempo, procurando agrupar las funcionalidades
relacionadas en la misma \gls{iteracion}. Se trata de un proceso iterativo que 
evoluciona, ampliando o mejorando funcionalidades en cada ciclo.


\subsubsection{Primera Iteración: Prototipo}

En la primera iteración se creó un prototipo con el que se comprobó la
adecuación de la tecnologías escogidas para ayudar a definir la mayor parte de
la base de la arquitectura del sistema. Las funcionalidades implementadas
fueron lo más básicas posibles, con el objetivo de poder hacer una demostración
del prototipo al usuario/cliente.

El prototipo de la primera iteración se centró en el motor de plantillas y
en la lectura de la información Exif de las fotografías. Dado que existían
otros programas capaces de geoetiquetar imágenes a partir de un fichero \gls{gpx}, se
supuso que la entrada del prototipo era una plantilla \gls{kml} y un directorio
con ficheros \gls{jpeg} geoetiquetados. La salida del prototipo fue generar una capa
KML 2.2 con las fotografías de forma que pudiera ser visualizada en Google Earth.
El objetivo era comprobar si realmente KML y las extensiones de \textit{touring}
eran capaces de cubrir los requisitos mínimos. Así pues, las historias de usuario
completadas en la primera iteración fueron:
\begin{description}
 \item[El usuario debe ser capaz de definir plantillas KML] \hfill \\
 Creación de un sistema de plantillas en \gls{xml} basado en variables con el
 desarrollo del módulo \textit{sXMLTemplate} y el programa \textit{csv2xmlgen}.
 Se puede considerar este programa como un spin-off del prototipo inicial. El
 hecho de desarrollarse como módulo independiente proporciona la ventaja de que
 puede ser sustituido en el futuro por otro motor de plantillas.
 \item[Procesar la información Exif de las fotografías]\hfill \\
 Para procesar las imágenes \gls{jpeg} se creó la entidad \textit{PhotoPlace.DataTypes.geoPhotoData}
 encargada de leer datos Exif usando la librería \textit{pyExiv2} y copiar y/o escalar 
 las fotografías con la librería \textit{Pillow}.
\end{description}

El programa \emph{csv2xmlgen}\footnote{Web oficial del proyecto: \url{http://code.google.com/p/csv2xmlgen}}
permitió demostrar la viabilidad del proyecto, puesto que desarrollando una
plantilla base \gls{kml} y procesando la información \gls{CSV} se pudo generar una capa
para Google Earth en la que se visualizaron los datos e imágenes.


\subsubsection{Segunda Iteración: Refactorización de la Arquitectura}

La característica fundamental de esta iteración fue la refactorización del código
del prototipo para construir una arquitectura flexible. Se completaron las
siguientes historias de usuario:

\begin{description}
 \item[Procesar la información GPX] \hfill \\
 Creación de la entidad \textit{pyGPX}, un módulo independiente para leer
 documentos \gls{gpx}. Esta entidad modeliza todas las características del formato y
 permite disponer de una serie de operadores sobre los datos.
 \item[Refactorización de la arquitectura del prototipo] \hfill \\
 Se identificaron los eventos y acciones del sistema, se pueden equiparar con
 los casos del programa en \textit{PhotoPlace.Actions}. Se desarrolla el núcleo
 del proyecto para geoetiquetar las imágenes y generar ficheros \gls{kml} a partir
 de la plantilla.
 \item[Creación de la web para el proyecto] \hfill \\
 En la plataforma en línea Google Code, se crea el proyecto como
 \emph{\software}\footnote{Disponible en la URL \url{http://code.google.com/p/photoplace}}
 liberado como software libre bajo la licencia \gls{GPL} y se incorpora el
 código fuente a un repositorio centralizado \textit{Subversion}.
\end{description}


\subsubsection{Tercera Iteración: Interfaz Gráfica de Usuario}

Hasta la tercera iteración, el programa no disponía de una interfaz gráfica de usuario. 
En este \textit{Sprint} se completaron las siguientes historias centradas en las funcionalidades
relativas a interacción del usuario con el programa:

\begin{description}
 \item[Interfaz gráfica de usuario, \gls{GUI}] \hfill \\
 Creación de una GUI usando \textit{PyGTK} y la herramienta Glade para definir
 los ficheros \textit{GtkBuilder} en \gls{xml}. Esto permite desacoplar la definición
 de la interfaz del código. Además se ha definido una \gls{api} usando un patrón
 \textit{Facade} que permitirá la creación de otras interfaces en el futuro,
 desacoplando a su vez la capa de presentación del resto del programa.
 \item[Manual de usuario] \hfill \\
 Creación del manual de usuario en Castellano e Inglés en formato PDF,
 ambos disponibles en la web.
 \item[Internacionalización de los mensajes de la interfaz] \hfill \\
 Usando \textit{gettext} se han extraído todas las cadenas de texto y se han
 traducido al Gallego y Castellano. El Inglés es el idioma oficial del proyecto,
 por lo que no se necesita traducción.
 \item[Diseño del logotipo para la aplicación.]
\end{description}


\subsubsection{Cuarta Iteración: Sistema de Extensiones}

En esta iteración se desarrolló el soporte para las extensiones o \glspl{plugin}
junto con las implementación de las propias extensiones:

\begin{description}
 \item[Desarrollo del sistema de extensiones] \hfill \\
 Las extensiones son módulos Python que pueden ser cargados dinámicamente y
 responden a las notificaciones que se envían desde las acciones. Se usa
 el patrón observador para implementar dicha funcionalidad.
 \item[Extensión \textit{Tour}] \hfill \\
 Desarrollo del plugin para crear presentaciones aéreas mostrando las fotografías.
 Para ello se han usado las extensiones \textit{gx:tour} del estándar \gls{kml} 2.2 que
 se han detallado en el capítulo \ref{chp:conceptos}, \nameref{chp:conceptos}.
 \item[Extensión \textit{GPXData}] \hfill \\
 Plugin para dibujar los \glspl{track} (caminos) y \glspl{waypoint} y mostrar información
 del documento \gls{gpx}: kilómetros recorridos, tiempo empleado, etc.
 \item[Extensión \textit{Files}] \hfill \\
 Esta extensión permite al usuario añadir otros ficheros para ser referenciados
 desde la capa generada. Se puede ver como un soporte para ficheros adjuntos.
\end{description}


\subsubsection{Quinta Iteración: Versión Web}

La aplicación web pretende ofrecer un servicio al gran público para crear
presentaciones, compartirlas y visualizarlas. Por lo tanto, se puede ver como una
versión no profesional del software desarrollado hasta el momento. Por esa razón,
no se contemplará el caso de uso en el que el usuario puede seleccionar una plantilla
\gls{xhtml}, en este caso, la plantilla será fija -se usa la plantilla por defecto- con
dos variables predefinidas para cada fotografía: título y descripción. Además, se
suprime la mayoría de parámetros y opciones con el objetivo de no sobrecargar
al usuario con conceptos avanzados. Hasta la fecha de edición de la presente memoria,
el trabajo de desarrollo en esta última iteración se ha centrado en:

\begin{description}
 \item[Aplicación Web] \hfill \\
 Usando Django y jQuery con el protocolo \gls{ajax} (ver capítulo \ref{chp:tecnologia}, \mbox{\nameref{chp:tecnologia}}).
 Se ha implementado una versión web para la aplicación. La arquitectura antes
 definida ha sido de gran ayuda para desarrollar esta nueva versión sin grandes
 esfuerzos.
 \item[Memoria del proyecto] \hfill \\
 Elaborar la memoria que explique todas las fases de desarrollo de este proyecto
 y realizar la presentación ante el tribunal.
\end{description}


\subsection{Componentes del Diseño Guiado por Dominio}

Siguiendo la metodología explicada en el capítulo anterior basada en el libro
\emph{Domain-Driven Design}\footnote{\textit{Domain-Driven Design: Tackling
Complexity in the Heart of Software} de Eric J. Evans} y los conceptos de
la web oficial\footnote{\url{http://www.domaindrivendesign.org}}
se definen una serie de artefactos o bloques desde los que se construye un \gls{ddd}.

\begin{figure}
  \centering
  \includegraphics[scale=0.3,keepaspectratio=true]{model_driven_design.png}
  \caption[Componentes del \gls{ddd}]{Componentes del \gls{ddd}}
  \label{fig:model_driven_design}
\end{figure}

\begin{description}
 \item[Entidad.] Es un objeto que no es definido por atributos, sino por un
identificador único en el contexto que se trata. Por ejemplo en este proyecto,
una fotografía geolocalizada.
 \item[Objeto Valor] (\textit{Value Object}), un objeto que tiene atributos, pero no
tiene identificador en el contexto. Se emplean para transferir datos en la
comunicación entre procesos, funciones, en \glspl{api}, etc.
  \item[Servicio] es una funcionalidad que no pertenece a ningún objeto del dominio,
en ese caso se crea un nuevo objeto que no es del dominio y que tendrá la
responsabilidad de realizar esta funcionalidad.
  \item[Evento] es un objeto que define un evento, una acción que sucede en el
dominio que los expertos son capaces de identificar y describir.
  \item[Agregado] es una composición de objetos (entidades y objetos valor)
sobre el que se debe mantener unas ciertas condiciones y tiene una entidad raíz.
No hay acceso al contenido del Agregado, y todas las operaciones que se quieran
hacer sobre el contenido siempre  se harán a través de la entidad raíz.
  \item[Factoría] encapsula la creación de objetos complejos (en muchos casos
agregados) que deben ser objetos válidos (no nulos).
  \item[Repositorio,] un conjunto de métodos (definidos en interfaces) para recuperar
objetos del dominio. Es un punto de entrada para mantener constantes las
entidades y agregados.
\end{description}

El software resuelve los problemas de parte del dominio, constituyendo sólo una
pequeña parte del sistema, pero de gran importancia. Es necesario desacoplar el
modelo de las demás funciones para evitar la confusión con otros conceptos más
tecnológicos, por ello se define una \textbf{\gls{arquitectura en capas}}. Después se
revisan los tres patrones de elementos del modelo: \textbf{entidades},
\textbf{objetos valor} y \textbf{servicios}. Estos elementos del modelo se pueden
agrupar en \textbf{módulos}, con gran cohesión entre ellos y deben tener
un bajo acoplamiento con otros para facilitar la comprensión del sistema.


\subsubsection{Entidades y Agregados}

El diseño de la aplicación se basa en las siguientes entidades y agregados que
forman parte del dominio de la aplicación:

\begin{itemize}
 \item \textbf{\textit{geoPhotoData}}, que representa cada una de las fotografías 
 \gls{jpeg}, con la información Exif asociada.
 \item \textbf{\textit{GPX}}, agregado que representa un documento \gls{gpx} de 
 entrada compuesto por \mbox{\textbf{\textit{GPXPoint}}} y \mbox{\textbf{\textit{GPXTrack}}} 
 para representar los \glspl{track} (caminos) y \glspl{waypoint} (puntos).
 \item \textbf{\textit{kmlData}}, es un agregado de objetos que representa la capa 
 \gls{kml} de salida. Las entidades que lo componen son: el objeto valor 
 \textbf{\textit{sXMLTemplate}} y la entidad \mbox{\textbf{\textit{geoPhotoData}}}.
\end{itemize}


\subsubsection{Objetos Valor}

Los objetos valor no son entidades en el dominio, por lo que la única característica
que diferencia dos instancia de este tipo son los datos que transportan o contienen:

\begin{itemize}
 \item \textbf{\textit{sXMLTemplate}}, representa un objeto \gls{xml} que se utiliza como 
 plantilla para generar otros documentos XML en función de las variables de entrada. Se 
 usa para las plantillas de usuario \gls{xhtml} y para la plantilla principal \gls{kml} 
 del documento de salida.
 \item \textbf{\textit{State}}, es un agregado de objetos que gestiona la configuración 
 del programa. Todas las acciones del programa reciben este objeto para acceder a la 
 configuración y las entidades.
 \item \textbf{\textit{definitions}}, representa la configuración por defecto de la aplicación.
\end{itemize}


\subsubsection{Eventos}

Las acciones del sistema se determinaron a partir de los casos de uso de la
aplicación, descomponiendo estos en eventos del dominio. La idea es representar lo
que ha sucedido en el dominio y poder detectar cuándo se producen los eventos para
actuar en consecuencia.

Existen varias formas de implementarlas, pero lo más normal es tener clases que
representen cada evento del dominio y una fachada estática que permita disparar
los eventos:

\begin{itemize}
 \item \textit{DoTemplates}. Carga todas las plantillas \gls{kml} y \gls{xhtml} del usuario.
 \item \textit{LoadPhotos}. Procesa todas las fotografías proporcionadas.
 \item \textit{ReadGPX}. Analiza y procesa un fichero \gls{gpx} de entrada.
 \item \textit{Geolocate}. Georeferencia las fotografías usando correlación temporal.
 \item \textit{MakeKML}. Genera del documento KML.
 \item \textit{WriteExif}. Geoetiqueta las fotografías usando etiquetas Exif.
 \item \textit{SaveFiles}. Procesa las fotografías y generar el documento KML o \gls{kmz} final.
\end{itemize}


\subsubsection{Factoría de Servicios y Casos de Uso}

Los servicios de la aplicación son los responsables de conducir y coordinar el flujo
de las acciones. Están diseñados para soportar todos los casos de uso de la aplicación.
Estos servicios se agrupan en fachadas usando el patrón \textbf{Facade} definiendo un
\gls{api} o conjunto de servicios para interactuar en el dominio.

En este proyecto, el API de servicios de la aplicación incluye los métodos
para desarrollar los siguientes \textbf{casos de uso}:

\begin{itemize}
 \item Cargar o modificar las plantillas \gls{xhtml} para procesar los datos de cada fotografía.
 \item Seleccionar un directorio con fotografías para ser procesadas.
 \item Seleccionar, cargar y analizar un documento \gls{gpx} de entrada.
 \item Geoetiquetar las fotografías usando el documento GPX.
 \item Seleccionar las extensiones a aplicar para generar la presentación.
 \item Generar la capa \gls{kml} de presentación con las imágenes.
\end{itemize}


\subsubsection{Repositorios}

Este patrón es el encargado de la capa de persistencia en las aplicaciones. En esta
aplicación, no es necesario una base de datos para guardar las entidades
o agregados del dominio. Sin embargo, para la versión web, dada su naturaleza basada en
sesiones, es necesario definir objetos \textit{wrapper} o adaptadores que mantengan las
referencias a las entidades reales. Más adelante, en el apartado dedicado a la versión Web
se detallará este uso.


\section{Arquitectura del software}

A continuación se detallan los componentes que conforman el núcleo del software
hasta la quinta \gls{iteracion} del proyecto.


\subsection{Módulos Independientes}

Durante el desarrollo del proyecto, se han identificado dos componentes que se
han implementado de forma independiente. Forman parte del proyecto, pero su
funcionalidad es lo suficientemente relevante dentro del dominio de la aplicación
que se ha decidido reaprovechar estas librerías para otros programas.
Por esa razón, los objetos que manejan las siguientes librerías son objetos de
negocio, es decir, tipos de entidades inteligible relevantes dentro de la capa
de negocio de la aplicación.


\subsubsection{Modelo GPX: \textit{PyGPX}}

Este módulo maneja objetos \gls{gpx}. Como se ha visto anteriormente,
esta entidad constituye un modelo del dominio, es decir, incorpora  tanto datos
y atributos junto con el comportamiento asociado para realizar operaciones sobre
sus datos. Este tipo de objetos representan la estructura de un fichero GPX,
proveyendo métodos para leer y extraer información contenida en un documento. \\

Como se puede apreciar en la figura \ref{fig:gpx}, que representa el diagrama de
clases del modelo, el esquema es muy parecido al patrón de diseño estructural 
\textit{Composición}. Sin embargo, las características del formato GPX impiden 
el uso del patrón puro.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.37,keepaspectratio=true]{pyGPX.png}
  \caption[Diagrama de Clases del Modelo GPX: \textit{PyGPX}]{Diagrama de Clases del Modelo GPX: \textit{PyGPX}}
  \label{fig:gpx}
\end{figure}

Desde el punto de vista del dominio, la clase \textit{GPXParser} es una factoría que
encapsula la creación de objetos agregado clase \textit{GPXItem} compuestos por:

\begin{description}
 \item[\textit{GPXPoint}] modeliza los \glspl{waypoint} (localizaciones del usuario) y
 puntos que componen las dos siguientes entidades.
 \item[\textit{GPXSegment}] es una lista temporalmente ordenada de puntos que
 representa parte de la siguiente entidad.
 \item[\textit{GPXTrack}] es un conjunto ordenado de segmentos \textit{pyGPX.GPXSegment}
 que representa tanto las rutas (\glspl{route}) como los caminos (\glspl{track}).
\end{description}


\subsubsection{Modelo Plantilla XML: \textit{sXMLTemplate}}

Este módulo maneja las plantillas \gls{xml} que se usan tanto para generar el fichero \gls{kml}
final, como para generar el contenido a partir de los datos de las fotografías
y la plantilla XHTML definida por el usuario.

\textbf{\textit{kmlData}} es el modelo de dominio que hace uso de \textit{sXMLTemplate}
para procesar las fotografías

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5,keepaspectratio=true]{sXMLTemplate.png}
  \caption[Diagrama de Clases de \textit{sXMLTemplate}]{Diagrama de Clases de \textit{sXMLTemplate}}
  \label{fig:sXMLTemplate}
\end{figure}

\subsection{\textit{API} de Servicios}

Las acciones del sistema se corresponden con los eventos que recibe la aplicación
de los casos de uso determinados durante el diseño. Esto define un conjunto de servicios
en una interfaz fachada \textbf{\textit{Facade}}, la cual especifica todos los casos de 
uso de la aplicación, es pues el \gls{api} de servicios de la aplicación. Además, como se 
mostrará más adelante, también agrupa los métodos para gestionar las extensiones, constituyendo 
un patrón estructural. \textbf{\textit{UserFacade}} y \textbf{\textit{WebFacade}} son las 
implementación específicas para casos de uso sobre \gls{GUI} tradicionales e interfaces 
web. La diferencia entre las implementaciones radica en la gestión del fichero de configuración 
del programa, inexistente en el caso de la versión web. \\

\begin{figure}
  \centering
  \includegraphics[scale=0.3,keepaspectratio=true,angle=90]{actions.png}
  \caption[Diagrama de clases de las Acciones]{Diagrama de clases de las Acciones}
  \label{fig:actions}
\end{figure}

Como se puede observar en la figura \ref{fig:actions}, otra consideración
importante es que todas las acciones son especializaciones de la clase \textit{Observable},
implementando el patrón \textit{Observador}, lo cual permite definir observadores en
la fachada para cada método. Esta característica es importante, puesto que las acciones
se ejecutan en hilos independientes de forma asíncrona. Los detalles de la implementación
se mostrarán en el capítulo siguiente.

El objeto valor \textbf{\textit{State}} se crea durante la inicialización de la fachada
y es un agregado con una triple funcionalidad:
\begin{itemize}
 \item Guardar las referencias a las entidades: \textit{GeoPhoto},
 \textit{KmlData} y \textit{GPX}. Es pues, un agregado de entidades.
 \item Objeto modelo de la configuración del programa. Gestiona la validez
 de todas los parámetros del programa.
 \item Transferir información entre todas las instancias de la aplicación: aciones
 extensiones, interfaces de usuario, etc.
\end{itemize}
\newpage

\subsubsection{Extensiones: \textit{PluginManager}}

El sistema de extensiones está gestionado por la clase \textbf{\textit{PluginManager}}, que se
encarga de cargar, inicializar y terminar los objetos y enviar las notificaciones
de las acciones. También se usa la estructura del patrón observador con la restricción
de \textit{singleton} para asegurarse que sólo una única instancia existe. Las extensiones
son especializaciones de la clase \textbf{\textit{Plugin}}, los métodos se suscriben
a los eventos de las acciones que desean observar.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.48,keepaspectratio=true]{plugin.png}
  \caption[Diagrama de Clases del Sistema de Extensiones]{Diagrama de Clases del Sistema de Extensiones}
  \label{fig:plugin}
\end{figure}

En la figura siguiente se muestra un diagrama de secuencia para varios servicios (casos de uso)
definidos en el \gls{api} de servicios. Por simplicidad no se muestran todas las notificaciones
que emiten las acciones\footnote{En el capítulo \ref{chp:implementacion}, \nameref{chp:implementacion},
apartado \ref{sub:notificaciones} se muestran los tipos de notificaciones},
solo se muestra una de ellas (indeterminada), el resto siguen la misma secuencia.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5,keepaspectratio=true]{user.png}
  \caption[Diagrama de secuencia para las Acciones]{Diagrama de secuencia para las Acciones}
  \label{fig:user}
\end{figure}

Se puede apreciar como el objeto valor \textbf{\textit{State}} es creado por la
fachada \textbf{\textit{UserFacade}} y guarda las referencias a los objetos entidad
\textbf{\textit{GeoPhoto}}, \textbf{\textit{KmlData}} y \textbf{\textit{GPX}}
cargados en los tres primeros casos de uso mostrados.


\subsection{Interfaz de Usuario en \textit{GTK+}}

Es la capa de la arquitectura del software con la que interactúa el usuario. Aparte de la
aplicación web que se detalla en la siguiente sección, las interfaces de usuario en este
proyecto siguen la implementación del patrón \gls{MVP}, Modelo-Vista-Presentador:

\begin{itemize}
 \item \textbf{Vista}, compuesto por los controles \gls{GTK} (\glspl{widget}) que forman la \gls{GUI}.
 \item \textbf{Modelo}, la lógica de negocio. En este caso son los servicios que ofrece la \textit{UserFacade}.
 \item \textbf{Presentador}, escucha los eventos que se producen en la vista (interacciones del usuario) y
 ejecuta las acciones necesarias a través del modelo. Además accede a las vistas a través de las interfaces
 que la vista debe implementar.
\end{itemize}

El hecho de que el presentador gestione la vista, es un derivado del patrón que se conoce como
\textbf{Vista Pasiva}:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.7,keepaspectratio=true]{mvp.png}
  \caption[Patrón Modelo-Vista-Presentador]{Patrón Modelo-Vista-Presentador}
  \label{fig:mvp}
\end{figure}

El hecho de que la fachada de servicios implemente a su vez el patrón observador, permite
que el presentador defina observadores para que reciban las notificaciones de las acciones,
dado que éstas se ejecutan asíncronamente.


\subsection{Aplicación Web}
\label{sec:django}

A diferencia del modelo de aplicación tradicional para computador personal, la pretensión es
aprovechar el trabajo realizado hasta el momento para desarrollar un servicio web que
permita a los usuarios de ámbito general, no expertos en el dominio, disfrutar de este proyecto.
Por ello, se puede considerar la aplicación de escritorio como la versión profesional
y la aplicación web como un servicio para crear presentaciones con fotografías para
el gran público.

Como se ha comentado, la versión web se ha desarrollado con Django y jQuery usando
\gls{ajax} con el objetivo de ofrecer un aplicación dinámica y atractiva para el usuario.
Para diferenciar el dominio de la aplicación, se han diferenciado dos componentes en
Django, lo que ayuda a un desarrollo en paralelo a la vez que permitirá reaprovechar
cada uno de ellos en otras aplicaciones:

\begin{description}
 \item[\texttt{web}] es el módulo que se encarga principalmente de gestionar los usuarios y
 sus perfiles. Es decir, define el estilo, agrupa todos los componentes que no forman
 parte del dominio y se encarga de enlazar la sesión de usuario con la aplicación
 \textit{kml}.
 \item[\texttt{kml},] módulo que contiene la aplicación desarrollada en la última iteración del
 software. Es el que agrupa todos los casos de uso definidos anteriormente, excepto
 la selección de las plantillas \gls{xhtml} y los parámetros avanzados, pero adaptados al
 ámbito web. Además, se añade otro caso de uso adicional que consiste en guardar la
 presentación generada para posteriormente listarla junto con el resto de las
 presentaciones que ha creado un usuario registrado.
\end{description}


\subsubsection{Django: Modelo}

Dado que la aplicación hasta el momento no dispone de base de datos\footnote{La
persistencia viene dada por los archivos que representan los objetos en el
sistema de ficheros}, en este caso sí que es necesario definir una capa adicional
para la persistencia de los objetos del modelo. Para ello se emplea el patrón
estructural \textbf{Adaptador}. Este patrón transforma la naturaleza de
fichero de las entidades \textit{geoPhotoData}, \textit{GPX} y \textit{kmlData}
en objetos \emph{wrapper}\footnote{Objetos envoltorio, con características
adicionales para la aplicación web} persistentes de forma que los metadatos y
un enlace al fichero (para su localización) se guardan en base de datos.

\begin{description}
 \item[KML] es el objeto que representa la entidad \textit{kmlData}, es decir, la capa
 KML generada. Además guarda todos los parámetros que se han usado para generar
 la capa y las referencias al resto de objetos que lo integran.
 \item[KMLItem] es una clase abstracta que modeliza los componentes que agrupa \gls{kml}.
 \item[Picture] representa las fotografías del usuario, es un wrapper
 de la entidad \textit{geoPhotoData}.
 \item[GPX] es el objeto que representa un documento \gls{gpx} usado en la geolocalización,
 wrapper de la entidad con mismo nombre.
 \item[MP3] es el objeto \textit{wrapper} que representa los ficheros mp3 que el usuario
 asocia a la presentación.
 \item[UserProfile] es un objeto de la aplicación Web que permite
 gestionar las cuentas de usuario en dicha aplicación.
\end{description}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.33,keepaspectratio=true]{ER_django.png}
  \caption[Modelo Entidad-Relación en Django]{Modelo Entidad-Relación, reflejando los Objetos empleados en Django}
  \label{fig:erdjango}
\end{figure}

Django emplea un \gls{ORM} que permite abstraer la base de datos de la aplicación, 
de forma que ésta es compatible con distintos \gls{SGBD}, tales como MySQL o PostgreSQL 
especificando el \textit{driver} en la configuración de Django. En este sentido, el
desarrollo de la aplicación se ha realizado usando el driver SQLite\footnote{Un pequeño SGBD 
embebido en una libería, compatible con ACID, de forma que se enlaza en el programa principal, 
sin ser un proceso cliente-servidor. Muy util para desarrollo o pequeñas aplicaciones}. 
Además también proporciona flexibilidad ante cambios en el gestor de la base de datos, 
puesto que el esquema relacional en \gls{SQL} se crea automáticamente a partir de las 
clases definidas.


\subsubsection{Django: Vista}
\label{subsec:djangoMVC}

Django no sigue exactamente la convención de nombres del patrón \textit{\acrlong{MVC}},
de forma que una vista en Django, es el \textbf{Controlador}, es decir, es la parte encargada de
procesar y ordenar los datos obtenidos del modelo o bien ingresados por el usuario.
Para cada objeto del modelo se ha definido la siguiente \gls{api}:

\begin{itemize}
 \item \textit{Objecto}./ genera los componentes de la interfaz de usuario para gestionar
 cada objeto. Una vez creada la interfaz HTML en el navegador y cargadas las funciones jQuery, 
 entran en juego los siguientes métodos.
 \item \textit{Objecto}./\textit{list} obtiene vía \gls{ajax} la lista de objetos del mismo tipo.
 \item \textit{Objecto}./\textbf{create} permite crear mediante AJAX un objeto.
 \item \textit{Objecto}./\textbf{delete/\$ID} borra vía AJAX el objeto usando su identificador.
 \item \textit{Objecto}./\textbf{update/\$ID} modifica con AJAX el objeto con su identificador.
\end{itemize}


\subsubsection{Django: Plantilla}

Se corresponde con la \textbf{Vista} en el modelo \gls{MVC}. Se basa en la
definición de plantillas que serán procesadas para generar los componentes de interfaz
de usuario. En el \gls{api} definido en la sección anterior, cada Vista dispone de una
plantilla que genera los componentes HTML, \gls{css} con el framework Bootstrap, provee las
funciones Javascript usando el framework jQuery para gestionar las peticiones \gls{ajax}.

