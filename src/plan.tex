\chapter[Plan de Proyecto]{
  \label{chp:plan}
  Plan de Proyecto
}
\minitoc
\newpage

Como paso previo a la realización de un proyecto es necesario realizar una serie de
análisis que -dependiendo del tipo de sistema a desarrollar- determinen su
viabilidad y permitan intentar predecir los problemas y los riesgos que se
encontrarán durante su elaboración. Por ejemplo, en un proyecto de ingeniería,
si los problemas de desarrollo superan a las expectativas vinculadas a las
predicciones y previsiones, no compensaría el esfuerzo de implementación.
En este capítulo, se estudia el plan de proyecto para la propuesta realizada,
sin tener en cuenta las futuras ampliaciones.


\section{Análisis de Viabilidad}

Al emprender el desarrollo de un proyecto los recursos y el tiempo deben ser
realistas para su materialización a fin de evitar pérdidas económicas o
frustración profesional. La viabilidad y el análisis de riesgos están relacionados
de muchas maneras, si el riesgo del proyecto es alto, la viabilidad de producir
software de calidad se reduce. Así pues, en el presente capítulo aborda los siguientes
puntos:

\begin{itemize}
    \item \textbf{Descripción} formal del proyecto.
    \item \textbf{Objeto}, motivos y necesidades del usuario.
    \item \textbf{Objetivos}, declaración del objetivo final del proyecto.
    \item \textbf{Metodología}, introducción al proceso del ciclo de vida elegido.
    \item \textbf{Requerimientos y recursos necesarios} para alcanzar el objetivo.
    \item \textbf{Sistema de control} para el seguimiento de los hitos principales.
    \item \textbf{Elementos de riesgo} que pueden hacer inviable el proyecto.
    \item \textbf{Beneficios} esperados tras la realización del proyecto.
    \item \textbf{Conclusiones} y resoluciones finales.
\end{itemize}


\subsection{Descripción}

El objetivo del proyecto es el diseño y desarrollo de una aplicación \gls{mashup}
que permita generar capas con información geográfica para geolocalizar y presentar
de una forma visual e interactiva fotografías con datos externos. La motivación
del proyecto nace de la necesidad de automatizar la recopilación de fotografías con
información externa de índole geográfica, de forma que ambas queden georeferenciadas,
permitiendo generar presentaciones dinámicamente.

Estas presentaciones deben ser capas compatibles con el estándar \gls{KML} que 
permitan ser visualizadas en línea o en una aplicación compatible con el estándar, 
como por ejemplo, Google Earth. Si es necesario, el geoetiquetado de las imágenes 
será a partir de datos en formato \gls{GPX}, permitiendo además crear una ruta aérea 
que presente toda la información. El formato para mostrar los datos será configurable 
y definido en plantillas modificables por el usuario. El programa deberá ser
multiplataforma, extensible y ampliable para que su funcionalidad sea
fácilmente adaptable a otros sistemas.

Toda la documentación y software del proyecto, una vez rematado éste,
quedarán liberados bajo las licencias \gls{GFDL} y \gls{GPL}, respectivamente.

\subsection{Objeto}

Este proyecto surgió debido a la necesidad de generar automáticamente capas para
Google Earth con información sobre la recolección de muestras para un grupo de
biología. Muchas disciplinas científicas en trabajos de campo, necesitan recoger
datos en lugares concretos, para ello: se cogen las muestras, se miden las
parámetros de estudio y se toman algunas fotografías; posteriormente toda esa
información se guarda en bases de datos \acrshort{SIG}. El programa propuesto deriva 
de esa idea, pero con un enfoque más general, de forma que será útil para cualquier
persona que quiera desarrollar presentaciones de las fotografías tomadas, por ejemplo 
en un viaje. No obstante, al tratarse de un programa con una arquitectura abierta,
será fácil desarrollar extensiones que permitan extraer información de otras
fuentes como bases de datos o ficheros \gls{CSV}.

\paragraph{Las necesidades}básicas que demandan los usuarios son:
\begin{itemize}
 \item Procesado de fotografías para geoetiquetado de las mismas basándose en sus
 \glspl{metadato} y/o en la información de localización de ficheros \gls{gpx}.
 \item Generación de capas \gls{kml} con las fotografías y datos adicionales, basadas en
 plantillas \gls{xhtml} y editables por el usuario. Las capas KML permitirán realizar
 presentaciones animadas basadas en las fotografías y los datos introducidos.
 \item Arquitectura abierta, que permita crear extensiones. Se pretende que otros usuarios
 avanzados puedan desarrollar \glspl{plugin} que interactúen con otras aplicaciones.
 \item Facilidad de uso con una interfaz de usuario intuitiva y amigable junto con
 un manual de usuario.
 \item Multiplataforma, generando paquetes de software que permita instalarlo
 fácilmente en la distribución de referencia para Linux, Ubuntu 12.04 y en la
 plataforma Windows 7\textsuperscript{\textregistered}\ de Microsoft, ambas en arquitectura 
 de 64 bits (amd64 o X86\_64).
\end{itemize}

\paragraph{Restricciones}asumidas por los usuarios en este proyecto:
\begin{itemize}
    \item Dependencia con Google Earth/Maps. Aunque existen otros visualizadores de
    \gls{kml}, hoy por hoy, es el programa que más posibilidades proporciona. Por esa razón,
    las capas generadas estarán basadas en KML 2.2 y optimizadas para las extensiones
    del programa de Google.
    \item Será necesaria la conexión a Internet para que la visualización con
    Google Earth funcione, excepto si la zona geográfica en cuestión está guardada en
    caché.
    \item Para usar las extensiones de Google Earth/Maps es necesario un token de usuario.
    Para ello es necesario generar una cuenta de usuario en los servicios de Google.
    Dependiendo del número de peticiones, puede ser necesario pagar por el uso de dichos
    servicios. Es importante tener en cuenta esta característica en una aplicación web,
    puesto que con un tráfico elevado, la factura puede ser considerable. Por esta razón
    se deben tener en cuenta otras plataformas para futuro uso.
\end{itemize}


\subsection{Objetivos}

En resumen, los \textbf{objetivos} básicos acordados para la finalización del
proyecto son, ordenados por prioridad:

\begin{enumerate}
    \item Construir un programa capaz generar capas \gls{kml} 2.2 que permitan ser
    visualizadas en línea o en una aplicación compatible con dicho estándar,
    como por ejemplo, Google Earth.
    \item El geoetiquetado de las imágenes será a partir de datos en formato \gls{gpx},
    o, si ya se encuentran geoetiquetadas, se usarán dichos metadatos para
    generar la presentación.
    \item El software deberá tener una interfaz gráfica de usuario sencilla e
    intuitiva. Además deberá estar preparado para distintos idiomas.
    \item El formato para mostrar las fotografías será configurable y definido en
    plantillas. Los usuarios podrán ajustar parámetros, definir y editar dichas
    plantillas para las presentaciones.
    \item El programa deberá ser multiplataforma, basado en capas, modular y ampliable
    a través extensiones (\glspl{plugin}) para integración con otros sistemas.
    \item El desarrollo del software se llevará a cabo usando un sistema de control
    de versiones en línea a través de Internet, abierto y al alcance de todo el mundo.
    \item Elaboración de un manual de usuario y de la memoria de proyecto.
    \item Generación de paquetes o instaladores para el programa (dependiendo del
    sistema operativo).
\end{enumerate}

Además de estos objetivos, si bien el software demandado es para uso en un
computador personal (\textit{PC}), durante el desarrollo del proyecto se tendrá
en cuenta la elaboración de una versión web. No es un requisito imprescindible,
pero se considera que dicha versión daría una mejor aceptación y visibilidad
mayor del software, alcanzando a mayor número de usuarios y plataformas.


\subsection{Metodología del Ciclo de Vida}

Dada la naturaleza de este proyecto, el proceso de desarrollo será dinámico y
evolutivo, es decir, a lo largo del tiempo, el software irá  evolucionando
añadiendo y ampliando características hasta llegar a la versión final. Este
método de ingeniería basado en el desarrollo iterativo e incremental y con la
participación activa del usuario o cliente, se conoce como desarrollo ágil.
Las \glspl{metodologia agil} se caracterizan por una serie de
\emph{principios}\footnote{Los principios del Manifiesto Ágil:
\url{http://www.agilemanifesto.org/iso/es/manifesto.html}}:

\begin{description}
 \item[Individuos e interacciones] sobre procesos y herramientas
 \item[Software funcionando]sobre documentación extensiva
 \item[Colaboración] con el usuario sobre negociación contractual
 \item[Respuesta ante el cambio] antes que seguir un plan estático.
\end{description}

El software desarrollado en una unidad de tiempo es llamado \textbf{\gls{iteracion}},
la cual típicamente dura de una a cuatro semanas. Cada iteración del ciclo de
vida global incluye: planificación, análisis de requerimientos, diseño,
codificación, revisión y documentación, siguiendo la metodología del \textbf{\gls{ddd}}.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.6,keepaspectratio=true]{desarrollo_software.jpg}
  \caption[Una Iteración del ciclo de vida]{Una Iteración del ciclo de vida}
  \label{fig:desarrollo_software}
\end{figure}

Como se verá en la siguiente sección, este es un enfoque para el desarrollo de
software con necesidades complejas mediante una profunda conexión entre la
implementación y los conceptos del modelo y núcleo del negocio. Las premisas son:

\begin{itemize}
 \item Establecer el foco primario de la aplicación en el núcleo y la lógica del
 dominio.
 \item Definir los diseños complejos con un modelo que describa el contexto.
 \item Colaboración entre técnicos y expertos del dominio para comprender los
 conceptos fundamentales del problema.
\end{itemize}

El modelo es una proyección del dominio. Esta metodología descarta la clásica
separación entre análisis, diseño e implementación, en favor de un modelo
evolutivo que abarque estas fases. Así, el \gls{ddd} ha
convertido el desarrollo de software en un proceso iterativo de refinamiento
del modelo, refinamiento del diseño y refinamiento del código, como una sola
actividad indivisible.


\subsection{Requerimientos y Recursos}

Para la gestión del proyecto se usará la plataforma en línea de Google Code\textsuperscript{\textregistered}
\footnote{\url{https://code.google.com}} para desarrolladores. Dicha plataforma proporciona
Git para el control de versiones, una herramienta \gls{wiki} para la elaboración
de la documentación técnica y la web principal, así como un sistema de gestión
de incidencias, mejoras y problemas. Además, permitirá la distribución de la
aplicación a través de Internet.

\paragraph{Requerimientos software}para alcanzar el objetivo final:
\begin{itemize}
  \item El entorno de desarrollo se basará en un ordenador personal con Debian GNU/Linux
  con Geany (\acrshort{IDE}\footnote{Integrated Development Environment, Entorno de Desarrollo
  Integrado, un conjunto de herramientas de programación que normalmente incluye: editor,
  compilador y depurador.} para la programación en Python) y una colección de
  imágenes y ficheros GPX sincronizados para realizar tests. También será necesario
  el sistema operativo Microsoft Windows 7\textsuperscript{\textregistered} para implementar
  el instalador y comprobar la compatibilidad del software en esta plataforma.
  \item Herramienta de virtualización Vagrant \footnote{\url{http://www.vagrantup.com}} para
  realizar test y desarrollo del instalador/paquete de distribución.
  \item Acceso y cuenta de usuario en la plataforma Google Code para control
  de versiones y desarrollo.
  \item La aplicación de Google Earth\textsuperscript{\textregistered} se usará como
  referencia para la visualización de la capas KML generadas (tanto en su versión Windows,
  como Linux).
  \item \LaTeX\ y LibreOffice\textsuperscript{\textregistered} o \emph{impress.js}\footnote{\url{http://bartaz.github.io/impress.js}} 
  para la realización de la memoria y presentación final del proyecto.
\end{itemize}

El tiempo máximo para el desarrollo asciende al período de un curso académico,
aproximadamente 9 meses, con dedicación exclusiva los últimos 6 meses y una única
persona, que deberá desempeñar todos los roles, planificando y realizando todas las 
fases e iteraciones del proyecto.

\paragraph{Recursos,}documentación en línea:
\begin{itemize}
  \item \textbf{\acrshort{KML}} Manual de referencia en línea: \newline
  \url{https://developers.google.com/kml}
  \item \textbf{\acrshort{GPX}} Especificaciones y ejemplos \newline
  \url{http://www.topografix.com/gpx.asp}
  \item \textbf{\acrshort{Exif}} Información del formato \newline
  \url{http://www.exif.org}
\end{itemize}

En cuanto a los recursos propios, se cuenta con:
\begin{itemize}
    \item Computador personal con dos sistemas operativos instalados
    \textit{Windows 7 Home Edition}\textsuperscript{\textregistered} y distribución 
    \emph{Debian\textsuperscript{\textregistered} GNU/Linux} en su versión
    \textit{testing} con conexión a Internet.
    \item Conjunto de ejemplos de documentos GPX y un conjunto de fotografías
    en formato \gls{JPEG} tomadas simultáneamente con alguna de las capas GPX.
\end{itemize}

En general todos los recursos software empleados se caracterizarán por ser de
código abierto, normalmente distribuidos bajo la licencia \gls{GPL}.


\subsection{Sistema de Control}

El sistema de control de este proyecto está basado en un calendario de actuación,
es decir, se controlará el tiempo invertido en cada \gls{iteracion}. Cada iteración se
corresponde con una fase o hito que amplia la versión anterior en un conjunto de
funcionalidad acordadas.

El tiempo total de realización es de 9 meses, con fecha limite de finalización la
fecha de entrega de Proyectos Fin de Carrera para el curso académico actual en la
Facultad de Informática de la Universidad de A Coruña.

Se establecen una serie de hitos que, en su parte de desarrollo, se corresponderán
con los ciclos de iteraciones del proyecto:
\begin{enumerate}
    \item \textbf{Plan de proyecto y Viabilidad}, que incluye la búsqueda de información
    de proyectos similares, evaluar posibles tecnologías para el desarrollo del
    proyecto, etc. La fase termina con un análisis de viabilidad y, si éste es
    positivo, se elabora un \emph{Plan de proyecto}. Dada la naturaleza de este proyecto,
    propuesto por el alumno en colaboración con los usuarios/clientes, esta fase se
    elaboró antes de la propuesta, por esa razón el tiempo invertido en esta fase no
    entra dentro del tiempo máximo de realización, no obstante se ha estimado en 5 semanas.
    \item \textbf{Análisis}. Tras la \emph{Documentación}, se procede al análisis
    de toda la información, se evalúa el uso de distintas tecnologías, se eligen
    las más adecuadas y se trazan las líneas maestras del proyecto en colaboración
    con los usuarios. Es decir, se establece la arquitectura global del software para
    cumplir los requerimientos. Tiempo estimado: de 4 a 5 semanas.
    \item \textbf{Ciclos de diseño, implementación y pruebas}. En cada iteración del
    ciclo de vida se definen las funcionalidades en una serie de fases: diseño,
    implementación y pruebas. En la fase de \emph{diseño} se priorizan y eligen las
    funcionalidades y se comprueba que cumple con los requisitos. Posteriormente en la
    fase de \textit{implementación} se  desarrollan las funcionalidades y por último, las
    pruebas. Al final de cada iteración se realizará una demostración al usuario.
    Se estiman 4 iteraciones de dos semanas de duración cada una para alcanzar los
    requisitos mínimos; en total 8 semanas.
    \item \textbf{Memoria}, que desarrolla el documento final que describirá todas las fases del
    del proyecto. Tiempo estimado para esta fase, 4 semanas.
\end{enumerate}

El método de trabajo se basará en la aplicación de un proceso continuo, excepto en las
fases de diseño, implementación y pruebas, que evolucionan cíclicamente. Dicho
de otro modo, las fases lineales son \emph{Plan de proyecto y Viabilidad} y
\emph{Análisis}, esto significa que ninguna de estas fases puede comenzar hasta que no
haya terminado la anterior. Al final de cada una de estas dos fases se elabora un plan que
define la actuación en fases sucesivas.

El ciclo evolutivo iterativo e incremental basado en \gls{metodologia agil} -apoyado en la
arquitectura modular del software- engloba las partes de \emph{Diseño}, \emph{Implementación}
y \emph{Pruebas}. La documentación de usuario también formará parte de una iteración.

Por último, se elaborará la memoria de proyecto.


\subsection{Elementos de Riesgo}

El principal riesgo del proyecto es el no cumplimiento de los hitos anteriores
en el tiempo indicado y considerando que los plazos de entrega son fijos y marcados
por el curso académico.

Otros posibles riesgos proceden de:
\begin{itemize}
  \item  La utilización de librerías externas, que pueden presentar fallos de programación
(\textit{bugs}) o incompatibilidades.
  \item La falta de conocimiento del desarrollador principal del proyecto en las
  metodología y en las tecnologías que se pretenden usar.
  \item Un único desarrollador del proyecto trabajando a tiempo parcial, jugando los
  roles de: \gls{Scrum Master} (Jefe de Proyecto), analista y desarrollador. Cualquier 
  infortunio puede afectar gravemente a los plazos propuestos.
\end{itemize}

No se considera que los posibles cambios en los requerimientos por parte del
usuario/cliente supongan un riesgo, puesto que la metodología de desarrollo elegida
se caracteriza por una comunicación permanente con el usuario. Además, participará en el
ciclo de iteraciones del proyecto.

Por estas circunstancias, se estima que en conjunto los plazos de las distintas fases
por las que atraviesa el proyecto, pueden acumular un retraso máximo total de un mes.


\subsection{Beneficios}

El objetivo de este proyecto no es conseguir beneficios económicos, se pretende
enfocar el proyecto de desarrollo de software como un proceso de ingeniería como
método de aprendizaje. Adicionalmente, se pretende cubrir las necesidades
de los usuarios anteriormente expuestas.

Sin embargo, las expectativas del este proyecto son altas. Tal y como se comentó
anteriormente, al auge de aplicaciones \gls{mashup} y a su vez la escasez de
aplicaciones que aglutinen todas las funcionalidades propuestas, auguran un
futuro provechoso de este proyecto.


\subsection{Conclusiones del Análisis}

La viabilidad y el análisis de riesgo están relacionados de muchas maneras.
Si el riesgo del proyecto es alto la viabilidad de producir software de calidad
se reduce. Las conclusiones extraídas se agrupan en tres partes diferenciadas:

\begin{description}
    \item[Viabilidad económica.] En el caso de este proyecto no es necesario
    un estudio de viabilidad económica puesto que no existen beneficios ni
    justificaciones económicas. Además, se trata de un proyecto de software libre,
    no se pretenden obtener réditos económicos con el proyecto propuesto.
    \item[Viabilidad técnica.] La viabilidad tecnológica es frecuentemente el
    área más difícil de valorar en esta etapa del proceso. Es esencial que el
    proceso de análisis y definición se realice en paralelo con una valoración
    de la viabilidad técnica. De todas formas, en este caso la viabilidad técnica
    es positiva, puesto que se trata de un proyecto de integración de las tecnologías
    que se exponen en el siguiente capítulo.
    \item[Viabilidad legal.] No se esperan problemas legales, se emplean formatos
    estándares y si se emplean librerías, serán con licencias abiertas y compatibles
    con la \gls{GPL} v3.
\end{description}


\section{Estado del Arte}

En esta sección se analizan las soluciones que se aplican hasta el momento a
problemas similares. Se presentan las herramientas con las que rivalizará el
software desarrollado.

\subsection{Antecedentes}

A día de hoy, no se ha encontrado realmente una herramienta completa que ofrezca
al usuario todos las funcionalidades que se proponen en el presente proyecto.
Por un lado, existen programas para geoetiquetar fotografías, guardando las
coordenadas en los \glspl{metadato} de la propia imagen. Por otro lado, existen otros
programas para generar capas \gls{KML} a partir de ficheros \gls{GPX}, pero sin posibilidad
de añadir fotografías. También se han encontrado algunas aplicaciones web que
permiten generar una capa KML con fotografías, pero, de forma manual y con gran cantidad
de publicidad a su alrededor, siendo tediosas y poco operativas.

\subsection{Proyectos Similares}

Como se ha comentado, se presenta pues, un dominio en el que existe un vacío de
aplicaciones automatizadas. Por esa razón, teniendo en cuenta todos los requisitos,
la única aplicación que rivaliza realmente con el proyecto es Google Earth\textsuperscript{\textregistered}.

\subsubsection{Google Earth}

Google Earth es un programa informático que muestra un globo virtual que
permite visualizar múltiples cartografías, basadas en imágenes satélite.

El programa fue creado bajo el nombre de EarthViewer 3D por la compañía
Keyhole Inc. La compañía fue comprada por Google en 2004 absorbiendo el
programa. El mapa de Google Earth está compuesto por una superposición de
imágenes obtenidas por satélites, fotografía aérea, información geográfica
proveniente de modelos de datos SIG de todo el mundo y modelos creados por
ordenador. Por esa razón, el programa es muy popular y ha sido descargado
más de mil millones de veces.

Está disponible en varias licencias, siendo gratis para uso personal, pero
limitándose a poco más que un simple visualizador en su versión básica.

El programa dispone de versiones para ser ejecutadas en ordenadores
personales bajo Windows, Mac OS X,distribuciones Linux y FreeBSD.
También existen versiones para dispositivos móviles Android, Apple IOS y
está disponible como una extensión para navegadores web.



\section{Metodología de Trabajo}

En un equipo distribuido en varias localizaciones es necesario para organizar
el trabajo de forma cuidadosa. Como se ha comentado, este proyecto se realizará
usando \glspl{metodologia agil}, pero mezclando características de varias de ellas
tal y como se describirá en esta sección. Los principios de las metodologías ágiles
son:

\begin{description}
 \item[Las personas son más importantes que los procesos y herramientas.] \hfill \\
Los desarrolladores son el principal factor de éxito de un proyecto software.
Construir un equipo es más importante que construir el entorno. Es mejor centrarse
en crear un equipo motivado y que éste configure su propio entorno de desarrollo
en base a sus necesidades.
 \item[Software que funcione sobre documentación exhaustiva.] \hfill \\
Desarrollar software que funciona en cada iteración por encima de escribir
documentación exhaustiva. La regla es “\textit{no producir documentos a menos que sean
necesarios de forma inmediata para tomar un decisión importante}”. Los documentos
deben ser cortos y centrarse en lo fundamental.
 \item[Colaboración con el cliente por encima de la negociación de contratos.] \hfill \\
La comunicación fluida con el usuario es más importante que las reuniones contractuales.
Se exige que exista una interacción continúa entre el usuario y el equipo
de desarrollo. Esta colaboración marcará la marcha del proyecto y asegurará su éxito.
 \item[La respuesta ante el cambio es más importante que seguir un plan.] \hfill \\
Responder rápidamente a los cambios más que seguir estrictamente un plan.
Es inevitable que surjan cambios a medida que se desarrollan \glspl{iteracion} y el
usuario sigue el desarrollo (cambios en los requisitos). También pueden surgir
cambios en las tecnologías, en el equipo, etc. por ese motivo, la planificación
no debe ser estricta sino flexible y abierta.
\end{description}


\subsection{Scrum y Kanban: Scrum-Ban}

Para el desarrollo estratégico, es decir la planificación de las \glspl{iteracion}, se
ha decidido usar la metodología \Gls{scrum}, mientras que para el trabajo
operativo del día a día se ha usado un flujo de trabajo \Gls{kanban}. Esta
mezcla de metodologías se conoce como \textbf{\gls{Scrum-Ban}}.

Para ello se definen historias, como las unidades de organización del proyecto.
Cada historia es una descripción concreta de un hecho (caso de uso, problema,
requisito) con una serie de tareas, pruebas y criterios de aceptación y un
esfuerzo. Las historias se organizan en columnas que indican su estado en el
flujo de trabajo, y éstas a su vez se organizan en grupos llamados
\textit{boards} (tablones de trabajo).

\paragraph{\Gls{scrum}}en cada iteración -también conocido como \textit{Sprint}, comienza
con una sesión de planificación. En una reunión, durante dicha sesión, se analiza
el trabajo en espera en el \textit{Backlog}, se prioriza y se deciden las
funcionalidades que se implementarán en la siguiente iteración. Además, en una
sesión previa, se han escrito las historias de usuario en el \textit{Backlog} y se
ha realizado una revisión de la iteración que ha finalizado, donde se repasa el
trabajo realizado, las razones por las que el trabajo pendiente no se ha
concluido y qué y cómo mejorar en la siguiente iteración. Como conclusión
a cada iteración también se realiza otra sesión de demostración al usuario/cliente
de las funcionalidades incorporadas en la iteración. En dicha reunión el cliente
puede opinar y participar abiertamente.

\paragraph{\Gls{kanban}}dentro de cada iteración. Se usa Kanban para el trabajo diario.
Al principio de cada jornada de trabajo se realiza una pequeña reunión para explicar
el trabajo que se ha realizado previamente y el trabajo que se va a realizar a
lo largo de la jornada y si se han encontrado problemas. Esta reunión es muy rápida,
sólo es una actualización del estado del trabajo a los responsables y se puede
realizar por videoconferencia. Si existen  otro tipo de problemas, se deben
proponer otras reuniones para ser tratados.


\subsection{Organización del Trabajo}

Para organizar el trabajo se usa la herramienta \emph{Trello}\footnote{Esta herramienta
web simula un tablero donde se colocan tarjetas agrupadas por columnas para organizar
las tareas. Más información en \url{http://www.trello.com}},
pero se podría organizar perfectamente usando tableros con tarjetas para cada historia.
Se han creado dos tipos de \textit{boards}: un \textit{backlog} y otro para la
iteración actual. Cada iteración dura dos semanas efectivas de trabajo.

En primer lugar, en el \textit{backlog} está representado todo el trabajo futuro,
pendiente de planificación, que será desarrollado en sucesivas iteraciones. Como
se ha comentado, se emplea Scrum como metodología. Se definen varias columnas para organizarlo:

\begin{itemize}
 \item \textbf{Épicas} son los hitos más grandes del proyecto, historias que se
 corresponden con requisitos del usuario que deben ser analizadas y divididas
 en historias menores.
 \item \textbf{Peticiones}, historias específicas que aparecen una vez comenzado el
 proyecto. Suelen ser cambios de requisitos del usuario tras las demostraciones.
 \item \textbf{Historias sin analizar} que deben ser completadas con una lista de tareas,
 definición de los requisitos de aceptación y pruebas para poder estar listas
 para la iteración.
 \item \textbf{Historias Listas para la Iteración}, totalmente definidas y completas.
 Además, tienen asociados unos puntos para determinar el esfuerzo a realizar. Se
 han definido cuatro tamaños para las historias: \textit{pequeña} (1 punto),
 \textit{mediana} (2 puntos), \textit{grande} (5 puntos) y \textit{xl} (10 puntos).
\end{itemize}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.7,keepaspectratio=true]{scrum.png}
  \caption{Metodología de trabajo}
  \label{fig:scrum}
\end{figure}

En segundo lugar, cada iteración dispone de un tablón donde se añade la lista
de historias completas seleccionadas para la siguiente iteración. El trabajo
se organiza siguiendo una metodología \textit{Kanban} tradicional, para ello se han
definido las siguientes columnas:

\begin{itemize}
 \item \textbf{\textit{Backlog} de Iteración} Lista de historias completas seleccionadas 
 para la presente iteración. Cada desarrollador elige una serie de historias para
 completar en la iteración.
 \item \textbf{En Espera}. Lista de historias que el desarrollador no puede completar
 por algún motivo ajeno a él. Una vez el motivo de espera ha desaparecido,
 se puede continuar con el trabajo.
 \item \textbf{Bloquedadas}. Historias que están bloqueadas por motivos internos al
 proyecto, se incluyen motivos como: falta de conocimiento en la tecnología,
 peticiones de ayuda, dependencias con otras tareas no desarrolladas,
 incumplimientos de contrato en \gls{API}/Interfaces, etc. Si una historia permanece
 demasiado tiempo en este estado, es motivo de una reunión.
 \item \textbf{En Proceso} son las historias cuyas tareas están siendo desarrolladas.
 \item \textbf{Realizadas}. Historias completadas con éxito en la iteración actual.
\end{itemize}

Mediante esta organización es posible controlar el trabajo realizado en cada
iteración. Además se emplean etiquetas que permiten reportar informes del
tipo de trabajo realizado frente al estimado. Cualquier evento extraordinario
es constitutivo de una reunión.


\subsection{Diseño Guiado por el Dominio}

Como se ha comentado previamente, este proyecto seguirá la filosofía del
\gls{ddd}, o \emph{Domain Driven Design}\footnote{El término
fue acuñado por Eric Evans en su libro \textit{Domain-Driven Design - Tackling
Complexity in the Heart of Software}} (DDD) para el ciclo de vida del software.
El Diseño Guiado por el Dominio no es una tecnología ni una metodología, provee
un conjunto de buenas prácticas y terminologías para tomar decisiones de diseño
que enfoquen y aceleren el manejo de dominios complejos en los proyectos de
software. El objetivo es crear un modelo del dominio rico que evoluciona a través
de sucesivas \glspl{iteracion} del diseño. Este enfoque está orientado a las metodologías
ágiles y supone que el desarrollo es iterativo y que existe una estrecha relación
entre los desarrolladores y los expertos del dominio.

Tal y como se puede apreciar en la figura \ref{fig:mdd}, los conceptos básicos 
del Diseño Guiado por el Dominio son:
\begin{description}
 \item[Dominio] es la esfera de conocimiento, ontología, área de aplicación del
 programa. En este caso, tal como se ha mencionado en el capitulo anterior, el
 dominio es la Geomática.
 \item[Modelo] es el conjunto de abstracciones que describen el modo en que se han
 representado y solucionado los problemas que intenta resolver el software relativos
 al dominio. En este capítulo se muestra el diseño de la aplicación y los
 distintos modelos.
 \item[Lenguaje ubicuo,] conceptos usados en el modelo y por todos los miembros del
 equipo, desde el usuario final hasta el desarrollador para describir todas las
 actividades y procesos que el software debe realizar.
 \item[Contexto] en que se define el significado de acciones y procesos.
\end{description}

\begin{figure}
  \centering
  \includegraphics[scale=0.42,keepaspectratio=true]{integridad_modelo.png}
  \caption[Bases del \gls{ddd}]{Bases del \gls{ddd}}
  \label{fig:mdd}
\end{figure}

La clave consiste en mantener el modelo y la implementación alineados:
\begin{itemize}
 \item Aislar el dominio. El software constituye una pequeña parte del sistema,
 pero muy importante, es necesario definir una \textbf{\gls{arquitectura en capas}}.
 \item Expresar el modelo mediante el software. Se usan tres patrones de elementos
 del modelo: \textbf{Entidades}, objetos que poseen una identidad propia que se
 debe preservar; \textbf{Objetos Valor}, objetos que no tienen identidad y que se
 emplean para comunicaciones y describen características; \textbf{Servicios},
 operaciones o acciones que conceptualmente no pertenecen a ningún objeto.
 Estos elementos del modelo se pueden agrupar en módulos, que deben contener
 conceptos con gran cohesión entre ellos y permiten tener un bajo
 acoplamiento con otros módulos para facilitar la comprensión del sistema.
 \item Gestionar el ciclo de vida de los objetos. Se definen varios patrones:
 un patrón \textit{agregado} que define un grupo de \textit{entidades} y
 \textit{objetos valor} sobre el que realizar ciertas operaciones; una
 \textit{factoría} encapsula la creación de objetos (entidades o agregados); un
 \textit{repositorio} permite definir un punto de entrada para acceder a los objetos.
\end{itemize}

Además del libro de referencia de Eric Evans, en la web oficial\footnote{\url{http://www.domaindrivendesign.org}} 
se agrupa abundante documentacion sobre \gls{ddd}. A su vez, los autores han elaborado
un ejercicio práctico\footnote{\url{http://dddsample.sourceforge.net}} 
con detalles de cómo desarrollar está metodología. En el caso de este proyecto, los detalles 
del Diseño Guiado por el Dominio realizado se analizarán en el capítulo ~\nameref{chp:disenho}.

